package br.com.fourop.fourfield;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class LoginResource {

	private final String TAG = "Resource-Login"; 
	
	public boolean authentic(){
		
		boolean login = false;		
		daoJson con = new daoJson();		
		String param = null;		
		
		param = Resources.getUsername();
		param += "/";
		param += Resources.getPassword() ;
		
		JSONObject data = null;
		
		try {
			
			data = con.connect(urlJson.getURL_JSON_LOGIN(), param);
			
			String data1 = data.getJSONObject("VerificarLoginResult").getString("Status");
			
			if (data1.equals("OK")){
				login = true;
			}
			
		} catch (ClientProtocolException e) {	
			Log.i(TAG, e.getMessage());
		} catch (IOException e) {
			Log.i(TAG, e.getMessage());
		} catch (JSONException e) {
			Log.i(TAG, e.getMessage());
		}

		return true;
		
	}
}