package br.com.fourop.fourfield;

public class urlJson {
	
	private static String ip_json = "https://4op.adamaseps.com.br/mobile/";
	private static String URL_JSON_LOGIN =  ip_json+"Gestao/ServicoFluxoOperacional.svc/VerificarLogin/";
	private static String URL_JSON_MAIN = ip_json +"Gestao/ServicoFluxoOperacional.svc/ObterTodos/";
	private static String URL_JSON_DET_OS = ip_json +"Gestao/ServicoFluxoOperacional.svc/ObterPorChave/";
	private static String URL_JSON_DET_EVOLUIR = ip_json +"Gestao/ServicoFluxoOperacional.svc/EvoluirFase/";
	private static String URL_JSON_INDICADOR = ip_json + "Gestao/ServicoFluxoOperacional.svc/ObterIndicador/";
	private static String URL_JSON_RECURSO = ip_json + "Gestao/ServicoFluxoOperacional.svc/MudarStatusRecurso/";
	private static String URL_JSON_OCORRENCIA = ip_json + "Gestao/ServicoFluxoOperacional.svc/AdicionarOcorrencia/";
	private static String URL_JSON_UPLOAD = ip_json + "Gestao/ServicoFluxoOperacional.svc/UploadFile?";
	
	public static String getURL_JSON_LOGIN() {
		return URL_JSON_LOGIN;
	}
	public static String getURL_JSON_MAIN() {
		return URL_JSON_MAIN;
	}
	public static String getURL_JSON_DET_OS() {
		return URL_JSON_DET_OS;
	}
	public static String getURL_JSON_DET_EVOLUIR() {
		return URL_JSON_DET_EVOLUIR;
	}
	public static String getURL_JSON_INDICADOR() {
		return URL_JSON_INDICADOR;
	}
	public static String getURL_JSON_RECURSO() {
		return URL_JSON_RECURSO;
	}
	public static String getURL_JSON_OCORRENCIA() {
		return URL_JSON_OCORRENCIA;
	}
	public static String getURL_JSON_UPLOAD() {
		return URL_JSON_UPLOAD;
	}

	
	
}
