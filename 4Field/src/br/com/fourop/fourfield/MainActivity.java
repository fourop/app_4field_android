package br.com.fourop.fourfield;

import com.example.login.R;

import android.app.Activity;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

	private WebView mWebView;
	private boolean isProgress = false;

	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

		mWebView = (WebView) findViewById(R.id.container);

		mWebView.setWebChromeClient(new GeoWebChromeClient());

		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setDisplayZoomControls(false);

		mWebView.setWebViewClient(new GeoWebViewClient());

		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setGeolocationEnabled(true);

		mWebView.getSettings().setPluginState(mWebView.getSettings().getPluginState().ON);

		mWebView.getSettings().setUseWideViewPort(false);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setSupportZoom(true); 

		startWebView("http://master.fourop.com.br/Fourop/get.jsp?email=" + Resources.getUsername() + "&pwd=" + Resources.getPassword() );
		setContentView(mWebView); 

	}

	private void startWebView(String url) {

		mWebView.setWebViewClient(new WebViewClient() {      
			ProgressDialog progressDialog;

			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			//Show loader on url load
			public void onLoadResource (WebView view, String url) {

				if (isProgress == false) {
					progressDialog = new ProgressDialog(MainActivity.this);
					progressDialog.setMessage("Carregando...");
					progressDialog.setCancelable (false);
					progressDialog.show();
				}

				// Logout
				if (mWebView.getUrl().contains("index.jsp")){
					startActivity(new Intent(MainActivity.this,Login.class));
				}else if (mWebView.getUrl().contains("main.jsp")){
					isProgress = true;

				}

			}

			public void onPageFinished(WebView view, String url) {

				try{

					if (progressDialog.isShowing ()) {
						progressDialog.dismiss();
						progressDialog = null;
					}

				}catch(Exception exception){
					exception.printStackTrace();
				}
			}

		}); 

		mWebView.loadUrl(url);

	}

	@Override

	public void onBackPressed() {
		if(mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			// Let the system handle the back button
			super.onBackPressed();
		}
	}

	public void openFileChooser( ValueCallback<Uri> uploadMsg, String acceptType ) {  
		this.openFileChooser(uploadMsg, acceptType);
	}

	@SuppressWarnings("unused")
	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			System.out.println(Uri.parse(url).getHost());

			if (Uri.parse(url).getHost().equals("http://192.168.1.14:8080/Fourop/main.jsp")) {
				// This is my web site, so do not override; let my WebView load the page

				System.out.println( " Main PAge!!!!! Porra ");

				return false;
			}
			System.out.println(Uri.parse(url).getHost());
			// Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
			startActivity(intent);
			return true;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)  && mWebView.canGoBack()) 
		{
			mWebView.goBack();
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}


	/**
	 * WebViewClient subclass loads all hyperlinks in the existing WebView
	 */
	public class GeoWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// When user clicks a hyperlink, load in the existing WebView
			view.loadUrl(url);
			return true;
		}
	}

	/**
	 * WebChromeClient subclass handles UI-related calls
	 * Note: think chrome as in decoration, not the Chrome browser
	 */
	public class GeoWebChromeClient extends WebChromeClient {
		@Override
		public void onGeolocationPermissionsShowPrompt(String origin,
				GeolocationPermissions.Callback callback) {
			// Always grant permission since the app itself requires location
			// permission and the user has therefore already granted it
			callback.invoke(origin, true, false);
		}
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
}
