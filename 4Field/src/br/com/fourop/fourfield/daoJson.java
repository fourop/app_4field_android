package br.com.fourop.fourfield;


import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.os.StrictMode;
import android.util.Log;
/**
 * 
 * @author Marcel
 *
 * <p> classe responsavel por gerenciar a conexao com o Json</p>
 * 
 */
public class daoJson {

	private final static String TAG = "daoJson"; 

	/**
	 * <p> medoto responsavel por chamar o Json
	 * 
	 * @param URL 
	 * <p> Informar a <b> URL </b> a ser chamada
	 * @param parametro
	 * <p> Informar os parametros da <b> URL</b>
	 * <p> Não é necessário enviar o <b>Login</b> </p>
	 * @return
	 * <p> Retorna um <b>JSONObject</b> da URL
	 */
	public static JSONObject loadJson(String URL, String parametro){
	
		String PARAM = null;
		daoJson con = new daoJson();
		JSONObject data = null;
		
		PARAM = Resources.getUsername();
		PARAM += "/";
		PARAM += Resources.getPassword();
		
		/** Somente adiciona parametro se tiver conteudo */
		if (parametro != null){
			PARAM += "/" + parametro;
		}
		
		try {

			data = con.connect(URL, PARAM);
			
		} catch (ClientProtocolException e) {	
			Log.i(TAG, e.getMessage());
		} catch (IOException e) {
			Log.i(TAG, e.getMessage());
		} catch (JSONException e) {
			Log.i(TAG, e.getMessage());
		}
		
		return data;
		
	}
	
	public JSONObject connect(String URL, String param) throws JSONException, ParseException, IOException{

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		JSONObject data = null;

		DefaultHttpClient client = new DefaultHttpClient();

		if(param != null){
			param = param.replaceAll(" ", "%20");
			URL += param;
		}

		HttpGet get = new HttpGet(URL.toString());

		Log.i(TAG, URL.toString());

		HttpResponse res = null;
		int status=0;


		try {
			res = client.execute(get);
		} catch (ClientProtocolException e1) {
			Log.i(TAG, "Error --> " + e1.getMessage());
		} catch (IOException e1) {
			Log.i(TAG, "Error --> " + e1.getMessage());
		}

		status = res.getStatusLine().getStatusCode();


		if (status==200){

			HttpEntity e = res.getEntity();
			String retJson = null;
			retJson = EntityUtils.toString(e);

			data = new JSONObject(retJson);

		}else{

			Log.e(TAG, Integer.toString(status));
		}


		return data;

	}


}