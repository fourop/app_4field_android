package br.com.fourop.fourfield;

import com.example.login.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnKeyListener{

	private static final String TAG = "Login";
	private EditText passwordField;
	private EditText usernameField;

	protected void onCreate(Bundle savedInstanceState){

		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_login);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		passwordField = (EditText) findViewById(R.id.login_password);
		usernameField = (EditText) findViewById(R.id.login_username);

		passwordField.setOnKeyListener((OnKeyListener) this);

	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {

		Resources.setUsername(usernameField.getText().toString());
		Resources.setPassword(passwordField.getText().toString());

		if( keyCode == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER ){		

			if(Resources.getPassword().length() != 0 || Resources.getPassword().length() != 0 ){

				LoginResource login = new LoginResource();

				if (login.authentic() == true){

					startActivity(new Intent(Login.this,MainActivity.class));
				}else{
					Toast.makeText(getApplicationContext(), "Login invalido ", Toast.LENGTH_SHORT).show();
				}
			
			}


		}else{

//			Toast.makeText(getApplicationContext(), "Login invalido 2", Toast.LENGTH_SHORT).show();

		}

		return false;
	}

}